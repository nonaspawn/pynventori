import abc
from typing import Tuple, Dict, List, Any

Field = str
Attribute = Tuple[Field, type]
Specification = Dict[Field, Any]


class Container:
    def __init__(self, blueprint: 'AbstractBlueprint', specification: Specification) -> None:
        self.blueprint = blueprint
        self.specification = specification

    def assemble(self) -> 'Storable':
        return self.blueprint.assemble(self.specification)

    def __repr__(self) -> str:
        return "<{}> {}".format(self.blueprint.get_storable_name(), self.specification)


class Storable(metaclass=abc.ABCMeta):
    @staticmethod
    @abc.abstractmethod
    def get_blueprint() -> 'AbstractBlueprint':
        pass

    def repackage(self) -> Container:
        blueprint = self.get_blueprint()
        specification = dict()
        for attribute in blueprint.get_attributes():
            specification[attribute[0]] = getattr(self, attribute[0])
        return Container(blueprint, specification)


class AbstractBlueprint(metaclass=abc.ABCMeta):
    @staticmethod
    @abc.abstractmethod
    def get_storable_class() -> type:
        pass

    @staticmethod
    @abc.abstractmethod
    def get_storable_name() -> str:
        pass

    @staticmethod
    @abc.abstractmethod
    def get_attributes() -> List[Attribute]:
        pass

    @staticmethod
    @abc.abstractmethod
    def assemble(specification: Specification) -> Storable:
        pass
