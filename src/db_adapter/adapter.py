import abc
from enum import Enum
from typing import Tuple, Iterable, List, Union

DBPath = str
DBType = str
Row = tuple
Column = Union[Tuple[str, type], Tuple[str, type, Iterable['Constraint']]]


class DBAdapter(metaclass=abc.ABCMeta):
    class Constraint(Enum):
        pass

    @staticmethod
    @abc.abstractmethod
    def open(database: DBPath) -> 'DBAdapter':
        pass

    @staticmethod
    @abc.abstractmethod
    def conform_type(type_: type) -> DBType:
        pass

    @abc.abstractmethod
    def create_table(self, name: str, columns: Iterable[Column]) -> None:
        pass

    @abc.abstractmethod
    def drop_table(self, name: str) -> None:
        pass

    @abc.abstractmethod
    def rename_table(self, old_name: str, new_name: str) -> None:
        pass

    @abc.abstractmethod
    def show_table(self, name: str) -> List[Row]:
        pass

    @abc.abstractmethod
    def add_column(self, table_name: str, name: str, type_: type,
                   constraints: Iterable[Constraint]) -> None:
        pass

    @abc.abstractmethod
    def insert_row(self, table_name: str, values: tuple) -> None:
        pass

    @abc.abstractmethod
    def commit(self) -> None:
        pass

    @abc.abstractmethod
    def rollback(self) -> None:
        pass

    @abc.abstractmethod
    def close(self) -> None:
        pass
