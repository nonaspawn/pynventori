import sqlite3
from typing import Iterable, Tuple, List, Union

from src.db_adapter.adapter import DBAdapter, DBType, DBPath, Row

Column = Union[Tuple[str, type], Tuple[str, type, Iterable['Constraints']]]


class SQLite3Adapter(DBAdapter):
    class SQLite3Constraint(DBAdapter.Constraint):
        NONE = ""
        PRIMARY_KEY = "PRIMARY KEY"
        NOT_NULL = "NOT NULL"
        UNIQUE = "UNIQUE"

    __CONVERTER_TYPES = {
        "None":            "null",
        "<class 'int'>":   "integer",
        "<class 'float'>": "real",
        "<class 'str'>":   "text",
        "<class 'bytes'>": "blob",
    }

    __DEFAULT_COLUMNS = [("id", int, (SQLite3Constraint.PRIMARY_KEY,))]

    @staticmethod
    def conform_type(type_: type) -> DBType:
        if str(type_) not in SQLite3Adapter.__CONVERTER_TYPES:
            raise TypeError("Type conformity for type '{}' not defined for {}.".format(
                type_.__name__, SQLite3Adapter.__name__))
        return SQLite3Adapter.__CONVERTER_TYPES[str(type_)]

    @staticmethod
    def open(database: DBPath = None) -> 'SQLite3Adapter':
        return SQLite3Adapter(database)

    def __init__(self, database: DBPath) -> None:
        self.database = database if database is not None else ":memory:"
        self.connection = sqlite3.connect(self.database)

    def create_table(self, name: str, columns: Iterable[Column] = list()) -> None:
        columns = list(
            map(lambda c: c + ([self.SQLite3Constraint.NONE, ],) if len(c) == 2 else c, columns))
        columns = columns if columns else self.__DEFAULT_COLUMNS
        columns_string = ", ".join(
            ["{} {} {}".format(
                name,
                self.conform_type(type_),
                " ".join([constraint.value for constraint in constraints]))
                for name, type_, constraints in columns])
        self.connection.execute("CREATE TABLE IF NOT EXISTS {} ({})".format(name, columns_string))

    def drop_table(self, name: str) -> None:
        self.connection.execute("DROP TABLE {}".format(name))

    def rename_table(self, old_name: str, new_name: str) -> None:
        self.connection.execute("ALTER TABLE {} RENAME TO {}".format(old_name, new_name))

    def show_table(self, table_name: str) -> List[Row]:
        return self.connection.execute(
            "SELECT * FROM {}".format(table_name)).fetchall()

    def add_column(self, table_name: str, name: str, type_: type,
                   constraints: Iterable[SQLite3Constraint] = (SQLite3Constraint.NONE,)) -> None:
        constraints_string = " ".join([constraint.value for constraint in constraints])
        self.connection.execute("ALTER TABLE {} ADD COLUMN {} {} {}".format(
            table_name, name, self.conform_type(type_), constraints_string))

    def insert_row(self, table_name: str, values: tuple) -> None:
        value_string = ",".join(["?" for _ in range(len(values))])
        self.connection.execute("INSERT INTO {} VALUES ({})".format(
            table_name, value_string), values)

    def commit(self) -> None:
        self.connection.commit()

    def rollback(self) -> None:
        self.connection.rollback()

    def close(self) -> None:
        self.connection.close()
