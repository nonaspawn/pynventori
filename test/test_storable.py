import unittest

from test.testclass import TestClass


class StorableTestCase(unittest.TestCase):
    def test_repackage_and_assemble(self) -> None:
        item = TestClass("lambda", 47, 0.23)
        expected_specification = {
            "str_attr":   "lambda",
            "int_attr":   47,
            "float_attr": 0.23,
        }
        container = item.repackage()
        self.assertDictEqual(expected_specification, container.specification)
        reassembled_item = container.assemble()
        for attribute in expected_specification:
            self.assertEqual(getattr(item, attribute), getattr(reassembled_item, attribute))
        self.assertDictEqual(container.specification, reassembled_item.repackage().specification)
