import unittest
from sqlite3 import OperationalError

from src.db_adapter.sqlite3 import SQLite3Adapter as Adapter


class DataManipulationTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.table_name = "test_manipulation"
        self.table_columns = [
            ("attr_int", int),
            ("attr_float", float),
            ("attr_str", str),
            ("attr_none", None),
            ("attr_bytes", bytes),
        ]
        self.db_adapter = Adapter.open()
        self.db_adapter.create_table(self.table_name, self.table_columns)

    def tearDown(self) -> None:
        self.db_adapter.commit()
        self.db_adapter.close()

    def test_insert_row(self) -> None:
        row = (47, 0.23, "lambda", None, 0)
        self.db_adapter.insert_row(self.table_name, row)
        self.assertTupleEqual(row, self.db_adapter.show_table(self.table_name)[0])


class DataDefinitionTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.db_adapter = Adapter.open()

    def tearDown(self) -> None:
        self.db_adapter.commit()
        self.db_adapter.close()

    def assertTableExists(self, table_name: str) -> None:
        self.db_adapter.show_table(table_name)

    def assertTableNotExists(self, table_name: str) -> None:
        self.assertRaises(OperationalError, self.db_adapter.show_table, table_name)

    def test_data_types_conformity(self) -> None:
        valid_types = {
            None:  "null",
            int:   "integer",
            float: "real",
            str:   "text",
            bytes: "blob",
        }
        invalid_types = [
            tuple,
            list,
        ]
        for type_, db_type in valid_types.items():
            self.assertEqual(Adapter.conform_type(type_), db_type)
        for type_ in invalid_types:
            self.assertRaises(TypeError, Adapter.conform_type, type_)

    def test_table_operations(self) -> None:
        table_name = "test_create"
        columns = [
            ("none_attr", None),
            ("int_attr", int, [Adapter.SQLite3Constraint.PRIMARY_KEY, ]),
            ("float_attr", float),
            ("str_attr", str, [Adapter.SQLite3Constraint.UNIQUE,
                               Adapter.SQLite3Constraint.NOT_NULL]),
            ("bytes_attr", bytes),
        ]
        self.assertTableNotExists(table_name)
        self.db_adapter.create_table(table_name, columns)
        self.assertTableExists(table_name)

        table_name = "test_add_column"
        column_name = "new_column"
        self.db_adapter.create_table(table_name)
        self.db_adapter.add_column(table_name, column_name, int)

        table_name = "test_drop"
        self.db_adapter.create_table(table_name)
        self.assertTableExists(table_name)
        self.db_adapter.drop_table(table_name)
        self.assertTableNotExists(table_name)

        old_table_name = "test_rename"
        new_table_name = "test_successful"
        self.db_adapter.create_table(old_table_name)
        self.assertTableNotExists(new_table_name)
        self.db_adapter.rename_table(old_table_name, new_table_name)
        self.assertTableNotExists(old_table_name)
        self.assertTableExists(new_table_name)


if __name__ == '__main__':
    unittest.main()
