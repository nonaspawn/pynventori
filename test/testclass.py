from typing import List, Type

from container import Storable, AbstractBlueprint, Specification, Attribute


class TestClass(Storable):
    class Blueprint(AbstractBlueprint):
        @staticmethod
        def assemble(specification: Specification) -> Storable:
            return TestClass(
                specification["str_attr"],
                specification["int_attr"],
                specification["float_attr"])

        @staticmethod
        def get_storable_class() -> type:
            return TestClass

        @staticmethod
        def get_storable_name() -> str:
            return "testclass"

        @staticmethod
        def get_attributes() -> List[Attribute]:
            return [
                ("str_attr", str),
                ("int_attr", int),
                ("float_attr", float),
            ]

    @staticmethod
    def get_blueprint() -> Type[AbstractBlueprint]:
        return TestClass.Blueprint

    def __init__(self, str_attr: str, int_attr: int, float_attr: float) -> None:
        self.str_attr = str_attr
        self.int_attr = int_attr
        self.float_attr = float_attr
