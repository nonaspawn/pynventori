import unittest

from inventory import Inventory
from test.testclass import TestClass


class InventoryTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.inventory = Inventory(":memory:")

    def test_store_and_retrieve(self) -> None:
        attributes = ("lambda", 47, 0.23)
        item = TestClass(*attributes)
        container = item.repackage()
        self.inventory.associate_class(item.get_blueprint())
        self.inventory.store(container)
        key = Inventory.Key(container.blueprint.get_storable_name())
        self.assertTupleEqual(attributes, self.inventory.show(key)[0])
        retrieved_container = self.inventory.retrieve(key)[0]
        self.assertEqual(container.blueprint, retrieved_container.blueprint)
        self.assertDictEqual(container.specification, retrieved_container.specification)
