from typing import List, Type

from container import AbstractBlueprint, Container
from src.db_adapter.adapter import Row
from src.db_adapter.sqlite3 import SQLite3Adapter


class Inventory:
    class Key:
        def __init__(self, storable_name: str) -> None:
            self.storable_name = storable_name

    Adapter = SQLite3Adapter

    @staticmethod
    def open(name: str) -> 'Inventory':
        return Inventory(name)

    def __init__(self, name: str) -> None:
        self.name = name
        self.connection = self.Adapter.open(name)
        self.blueprints = dict()

    def associate_class(self, blueprint: Type[AbstractBlueprint]) -> None:
        name = blueprint.get_storable_name()
        self.blueprints[name] = blueprint
        self.connection.create_table(name, blueprint.get_attributes())
        self.connection.commit()

    def show(self, key: Key) -> List[Row]:
        return self.connection.show_table(key.storable_name)

    def retrieve(self, key: Key) -> List[Container]:
        blueprint = self.blueprints[key.storable_name]
        attributes = blueprint.get_attributes()
        containers = list()
        for row in self.show(key):
            specification = dict()
            for i in range(len(row)):
                specification[attributes[i][0]] = row[i]
            containers.append(Container(blueprint, specification))
        return containers

    def store(self, container: Container) -> None:
        self.connection.insert_row(
            container.blueprint.get_storable_name(),
            tuple(container.specification.values()))

    def close(self) -> None:
        self.connection.close()
